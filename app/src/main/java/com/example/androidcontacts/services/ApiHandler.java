package com.example.androidcontacts.services;

import android.os.AsyncTask;
import android.util.Log;

import com.example.androidcontacts.Models.MyCallback;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class ApiHandler extends AsyncTask<String, Void, String> {
    private static final String TAG = "ApiHandler";

    private final MyCallback callback;

    public ApiHandler(MyCallback callback) {
        this.callback = callback;
    }


    @Override
    protected String doInBackground(String... strings) {
        String result = "";
        String apiUrl = strings[0];
        try{
            // handle to get data form backend
            URL url = new URL(apiUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            int responseCode = connection.getResponseCode();
            if(responseCode == HttpURLConnection.HTTP_OK){
                BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String line;
                while((line = reader.readLine()) != null){
                    result+=line;
                }
                reader.close();
            }else{
                Log.e(TAG, "doInBackground: Err: "+responseCode);
            }
            connection.disconnect();

        } catch (Exception e){
            Log.e(TAG, "doInBackground: "+e.getMessage());
        }
        return result;
    }

    @Override
    protected void onPostExecute(String s) {
        // Xử lý kết quả trả về ở đây
        callback.onCallback(s);
        super.onPostExecute(s);
    }
}
