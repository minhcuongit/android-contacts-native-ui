package com.example.androidcontacts.Models;

//"createdAt": "2023-06-02T12:15:38.702Z",
//"name": "Donald Beer",
//"avatar": "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/326.jpg",
//"address": "address 2",
//"phone": "phone 2",
//"id": "2"

public class ContactModel {
    String createdAt;
    String name;
    String avatar;
    String address;
    String phone;
    String id;

    public ContactModel() {
    }

    public ContactModel(String createdAt, String name, String avatar, String address, String phone, String id) {
        this.createdAt = createdAt;
        this.name = name;
        this.avatar = avatar;
        this.address = address;
        this.phone = phone;
        this.id = id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    @Override
    public String toString() {
        return "ContactModel{" +
                "createdAt='" + createdAt + '\'' +
                ", name='" + name + '\'' +
                ", avatar='" + avatar + '\'' +
                ", address='" + address + '\'' +
                ", phone='" + phone + '\'' +
                ", id='" + id + '\'' +
                '}';
    }
}
