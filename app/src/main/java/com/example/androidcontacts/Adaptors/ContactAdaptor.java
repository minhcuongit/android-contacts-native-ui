package com.example.androidcontacts.Adaptors;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.androidcontacts.Models.ContactModel;
import com.example.androidcontacts.databinding.RowBinding;

import java.util.ArrayList;

public class ContactAdaptor extends RecyclerView.Adapter<ContactAdaptor.ContactViewHolder> {
    private static String TAG = "CuongTran";

    private final ArrayList<ContactModel> listContacts;
    private final LayoutInflater inflater;

    public ContactAdaptor(ArrayList<ContactModel> listContacts, LayoutInflater inflater) {
        this.listContacts = listContacts;
        this.inflater = inflater;
    }

    @NonNull
    @Override
    public ContactViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ContactViewHolder(RowBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ContactViewHolder holder, int position) {
        ContactModel contactModel = listContacts.get(position);
        holder.bindTo(contactModel);
    }

    @Override
    public int getItemCount() {
        return listContacts == null ? 0 : listContacts.size();
    }

    public class ContactViewHolder extends RecyclerView.ViewHolder {
        private RowBinding binding;

        public ContactViewHolder(@NonNull RowBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            this.binding.getRoot().setOnClickListener(view -> {
                Log.d(TAG, "ContactViewHolder: on tap on " + binding.txtName.getText());
            });
        }

        public void bindTo(ContactModel model) {
            binding.txtName.setText(model.getName());
            binding.txtPhone.setText(model.getPhone());

        }
    }

}
