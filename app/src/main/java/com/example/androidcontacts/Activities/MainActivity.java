package com.example.androidcontacts.Activities;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.androidcontacts.Adaptors.ContactAdaptor;
import com.example.androidcontacts.Models.ContactModel;
import com.example.androidcontacts.Models.MyCallback;
import com.example.androidcontacts.databinding.ActivityMainBinding;
import com.example.androidcontacts.services.ApiHandler;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements MyCallback {
    private ActivityMainBinding binding;
    private ApiHandler apiHandler;

    private String apiUrl = "https://647a1009a455e257fa6441cb.mockapi.io/api/v1/contacts";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        binding.btnGetData.setOnClickListener(v -> {
            binding.layoutInitText.setVisibility(View.GONE);
            binding.layoutLoading.setVisibility(View.VISIBLE);
            // handle to call background handler
            apiHandler = new ApiHandler(this);

            apiHandler.execute(apiUrl);
        });
    }

    @Override
    public void onCallback(String result) {
        Gson gson = new Gson();
        Type personListType = new TypeToken<List<ContactModel>>() {
        }.getType();
        ArrayList<ContactModel> contactModels = gson.fromJson(result, personListType);
        ContactAdaptor adaptor = new ContactAdaptor(contactModels, getLayoutInflater());
        binding.rvContacts.setLayoutManager(new LinearLayoutManager(this));
        binding.rvContacts.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        binding.rvContacts.setAdapter(adaptor);
        binding.layoutLoading.setVisibility(View.GONE);
        binding.layoutContacts.setVisibility(View.VISIBLE);
    }
}